# Smart Connect
Repositório do software de controle e gerenciamento do projeto Smart Connect

[Documentação](/docs/tex/documento.pdf)

## Pastas
- esp8266: contém o código do firmware que roda no ESP8266
- raspberry: contém o código principal da unidade de controle e gerenciamento
- raspberry/scripts: ferramentas para deploy e teste da unidade de controle e gerenciamento
- raspberry/database: contém o script do banco de dados
- docs: documentação em geral (payload, etc)

## Configuração do arduino IDE (para compilar o firmware da ESP8266)

### Preencha o campo 'Additional Boards Manager URLs' com o link a seguir:
    https://arduino.esp8266.com/stable/package_esp8266com_index.json

![image](docs/images/preferences.png)

### Instale a library a seguir (Boards Manager e biblioteca ESP8266):

![boards](docs/images/boards-manager.png)

Após isso, na aba tools->board deverá aparecer a placa 'LOLIN(WEMOS) D1 R2 & mini'

### Dependências

* Adafruit DHT11 sensor library: https://github.com/adafruit/DHT-sensor-library
* Adafruit unified library: https://github.com/adafruit/Adafruit_Sensor
