#
# Script para clonar o repositório do smart connect e executar o script de configuração
#

# Sequência de caracteres de escape ANSI
YELLOW="\x1b[33;1m"
BLUE="\x1b[34;1m"
RED="\x1b[31;1m"
GREEN="\x1b[32;1m"
RESET="\x1b[0m"
MAGENTA="\x1b[35;1m"
CYAN="\x1b[36;1m"

# Exibe uma pergunta na tela
function ask()
{
    QUESTION=$1

    printf "$MAGENTA[?]$RESET $QUESTION"
}

# Exibe mensagem de sucesso na tela
function log_ok()
{
    echo -e "$GREEN[+]$RESET $1"
}

# Exibe mensagem de erro na tela
function log_error()
{
    echo -e "$RED[!]$RESET $1"
}

# Exibe mensagem de informação na tela
function log_info()
{
    echo -e "$BLUE[*]$RESET $1"
}

# Exibe uma mensagem de alerta na tela
function log_warning()
{
    echo -e "$YELLOW[!]$RESET $1"
}

function panic()
{
    log_error "$1\n"
    exit 1
}

function main()
{
    # Verifica o nível de permissão
    if [ $UID -ne 0 ]; then
        panic "execute este script como root (ex: sudo su)"
    fi

    v=$(git --help)
    if [ $? -eq 127 ]; then
        log_info "instalando o git ..."
        apt update
        apt install -y git
    fi

    if [ -d "smartconnect" ]; then
        rm -rf smartconnect -vv
    fi


    $(git clone https://gitlab.com/citara-sapientia/smartconnect.git)
    log_info "localização do código-fonte: $PWD/smartconnect"
    cd smartconnect/scripts
    ./config.sh
}

main
