#
# Script para configurar o raspberry
#


# Script para configurar apenas a rede sem fio

# Sequência de caracteres de escape ANSI
YELLOW="\x1b[33;1m"
BLUE="\x1b[34;1m"
RED="\x1b[31;1m"
GREEN="\x1b[32;1m"
RESET="\x1b[0m"
MAGENTA="\x1b[35;1m"
CYAN="\x1b[36;1m"

# Pasta de instalação do software de controle
install_folder="/bin/sc-control-unit"

sounds_folder="$install_folder/sounds"

# Link de download do software
software_link="https://gitlab.com/citara-sapientia/smartconnect/-/archive/dev/smartconnect-dev.zip"

# Caminho do código-fonte do projeto
source_code_path="../raspberry"

# Local de instalação dos serviços
system_service_dir="/etc/systemd/system"

# nome do serviço
system_service_name="sc-control-unit"

# Nome do arquivo de serviço
system_service_filename="$system_service_name.service"

# Exibe uma pergunta na tela
function ask()
{
    QUESTION=$1

    printf "$MAGENTA[?]$RESET $QUESTION"
}

# Exibe mensagem de sucesso na tela
function log_ok()
{
    echo -e "$GREEN[+]$RESET $1"
}

# Exibe mensagem de erro na tela
function log_error()
{
    echo -e "$RED[!]$RESET $1"
}

# Exibe mensagem de informação na tela
function log_info()
{
    echo -e "$BLUE[*]$RESET $1"
}

# Exibe uma mensagem de alerta na tela
function log_warning()
{
    echo -e "$YELLOW[!]$RESET $1"
}

function panic()
{
    log_error "$1\n"
    exit 1
}

# Cria um backup das configurações existentes
function create_backup()
{   
    if [ ! -e "$1.bak" ]; then
        cp "$1" "$1.bak" -vv
    else
        log_warning "já existe um backup do arquivo $1"
    fi
}

# Obtém as credenciais da rede wifi e salva num arquivo de configurações
function get_wpa_credentials_and_save()
{
    # Caminho do arquivo de configurações
    local WPA_SUPPLICANT_PATH="/etc/wpa_supplicant/wpa_supplicant.conf" 
    local answer=""
    local REBOOT_TIMEOUT=10

    log_info "configurando as credenciais de rede sem fio ..."

    # Obtém as credenciais da rede sem fio
    while [[ "$answer" != "s" && "$answer" != "S" ]];
    do
        read -p "Nome da rede wifi: " ssid
        read -p "Senha: " key

        # Verifica se o usuário digitou alguma informação
        if [[ -z "$ssid" && -z "$key" ]]; then
            log_warning "As credenciais foram deixadas em branco. Manterei a padrão"
            break
        fi

        log_info "Nome: $ssid"
        log_info "Senha: $key"
        ask "As informações acima estão corretas? (s/n)"
        read -n 1 answer
        echo ""
    done

    # Verifica se o arquivo de configuração já existe
    if [ ! -e "$WPA_SUPPLICANT_PATH" ]; then
        log_warning "O arquivo $WPA_SUPPLICANT_PATH não existe ..."
        touch "$WPA_SUPPLICANT_PATH"
    fi

    # Configura as credenciais
    SSID=$ssid
    PSK=$key

    # Dados do arquivo
local FILE_DATA="ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=BR

network={
    ssid=\"$SSID\"
    psk=\"$PSK\"
    key_mgmt=WPA-PSK
}"

    # Cria um backup do arquivo de configuração da conexão sem fio
    log_info "criando backup do arquivo antigo ..."
    create_backup "$WPA_SUPPLICANT_PATH"
    log_ok "backup finalizado"

    # Escreve o arquivo no disco
    echo "$FILE_DATA" > $WPA_SUPPLICANT_PATH

    log_info "O sistema será reiniciado em $REBOOT_TIMEOUT segundo(s) para concluir a instalação do software ..."
    
    # Aguarda 10 segundos
    sleep $REBOOT_TIMEOUT

    # Reinicia
    reboot
}

function command_exists()
{
    cmd=$($1 --help)

    if [ "$?" == "127" ]; then
        return 0
    fi

    return 1
}

function update_package_list()
{
    log_info "atualizando lista de pacotes ..."
    if ! { apt update 2>&1 || echo E: update failed; } | grep -q '^[WE]:'; then
        log_ok "lista de pacotes atualizada com sucesso"
    else
        panic "erro ao atualizar lista de pacotes"
    fi
}

function install_deps()
{
    # Atualiza a lista de pacotes 
    update_package_list

    # Inicia a instalação das dependências
    log_info "Instalando dependências ..."

    command_exists git
    if [ $? -eq 0 ]; then
        apt install -y git
    fi

    command_exists python3
    if [ $? -eq 0 ]; then
        apt install -y python
    fi

    command_exists wget
    if [ $? -eq 0 ]; then
        apt install -y curl
    fi

    command_exists unzip
    if [ $? -eq 0 ]; then
        apt install -y unzip
    fi

    command_exists pip3
    if [ $? -eq 0 ]; then
        apt install -y python3-pip
        apt install -y python-pip
    fi

    #
    #command_exists pulseaudio
    #if [ $? -eq 0 ]; then
    #    apt install -y pulseaudio
    #    apt install -y alsa-base
    #fi  
}

function trigger_autostart
{  
    local autostart_script = "/etc/rc.local"

    log_info "configurando autoinicialização do software de controle ..."
    systemctl enable "$system_service_name"

    # às vezes o systemctl não funciona
    if [ ! -e "$autostart_script.bak" ]; then
        create_backup "$autostart_script"
    fi
    
    echo -e "systemctl start sc-control-unit" > $autostart_script

    log_ok "configuração concluída"
}

# Cria um backup das configurações existentes
function create_backup()
{   
    cp "$1" "$1.bak" -vv
}

function prepare_workspace()
{
    # Cria a pasta de instalação do programa, caso não exista
    if [ ! -d "$install_folder" ]; then
        log_info "Criando pasta de instalação ..."
        mkdir $install_folder
    fi

    if [ ! -d "$install_folder/sounds" ]; then
        mkdir $install_folder/sounds
    fi

    if [ "$?" != "0" ]; then
        panic "Não foi possível criar a pasta: '$install_folder'"
    fi

    log_info "pasta atual: $PWD"

    # Local de instalação do código-fonte
    local file="$PWD/../raspberry/main.py"
    local code_path="$PWD/../raspberry/*.py"
    local sound_file="$PWD/../raspberry/sounds/*.mp3"

    if [ -e "$file" ]; then
        log_info "software de controle localizado em: $file"
    else
        panic "erro ao localizar código-fonte. Pasta: $file"
    fi

    # Copia todos os arquivos para a pasta de instalação
    log_info "instalando software ..."
    cp $code_path $install_folder -vv
    
    # Copia o arquivo de som
    cp $sound_file $sounds_folder -vv

    if [ ! -e $sound_file ]; then
        log_warning "o arquivo $sound_file não foi localizado"
    fi

    # cp: não foi possível obter estado de '/home/pi/smartconnect/scripts/../raspberry/*.py': Arquivo ou diretório inexistente
    if [ $? -ne 0 ]; then
        log_warning "nem todos os arquivos puderam ser copiados"
    fi
}

function create_service()
{
    local SERVICE_FILE_DATA="[Unit]\nDescription=SmartConnect control and management unit (Core)\n\n[Service]\nWorkingDirectory=$install_folder\nType=simple\nExecStart=/usr/bin/python3 $install_folder/main.py\nRestartSec=5\nRestart=always\nSuccessExitStatus=0\n\n[Install]\nWantedBy=multi_user.target"

    # Cria o arquivo de serviço
    echo -e $SERVICE_FILE_DATA > "$system_service_dir/$system_service_filename"

    # Configura a inicialização automática
    trigger_autostart

    # Carrega as atualizações feitas
    systemctl daemon-reload
}

function install_python_deps()
{
    pip3 install pyserial
    pip3 install rpi.gpio
    pip3 install picamera
}

function check_python_deps()
{
	local STATUS_OK="#deps_installed"
	local STATUS_NOT_FOUND="#deps_incomplete"
	local PY_SCRIPT="try:\n\timport RPi.gpio\n\timport picamera\n\timport serial\n\tprint(\"$STATUS_OK\")\n\texcept ImportError as err:\n\tprint(\"$STATUS_NOT_FOUND\")"
	local PY_SCRIPT_FILENAME="check_gpio.py"
	
	log_info "verificando bibliotecas do python"
	echo -e "$PY_SCRIPT" > $PY_SCRIPT_FILENAME
	status=$(python3 $PY_SCRIPT_FILENAME)
	
	if [ "$status" != "$STATUS_OK" ]; then
		log_info "instalando bibliotecas ..."
		pip3 install "rpi.gpio"
        pip3 install "picamera"
        pip3 install "pyserial"
		log_ok "instalação concluída"
	else
		log_ok "todas as bibliotecas estão instaladas"
	fi

    log_info "apagando arquivos temporários"
    rm $PY_SCRIPT_FILENAME -vv
    log_ok "deleção finalizada"
}

function set_config_var() {
  lua - "$1" "$2" "$3" <<EOF > "$3.bak"
local key=assert(arg[1])
local value=assert(arg[2])
local fn=assert(arg[3])
local file=assert(io.open(fn))
local made_change=false
for line in file:lines() do
  if line:match("^#?%s*"..key.."=.*$") then
    line=key.."="..value
    made_change=true
  end
  print(line)
end
if not made_change then
  print(key.."="..value)
end
EOF
mv "$3.bak" "$3"
}

function get_config_var() {
  lua - "$1" "$2" <<EOF
local key=assert(arg[1])
local fn=assert(arg[2])
local file=assert(io.open(fn))
local found=false
for line in file:lines() do
  local val = line:match("^%s*"..key.."=(.*)$")
  if (val ~= nil) then
    print(val)
    found=true
    break
  end
end
if not found then
   print(0)
end
EOF
}

function enable_camera() 
{
    # $1 = 0 desabilita a câmera
    # $1 = 1 habilita a câmera

    # Stop if /boot is not a mountpoint
    if ! mountpoint -q /boot; then
        panic "/boot não é um ponto de montagem"
    fi

    [ -e /boot/config.txt ] || touch /boot/config.txt

    if [ "$1" -eq 0 ]; then
        log_info "desabilitando a câmera ..."
        set_config_var start_x 0 /boot/config.txt
        sed /boot/config.txt -i -e "s/^startx/#startx/"
        sed /boot/config.txt -i -e "s/^start_file/#start_file/"
        sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"
    else
        log_info "habilitando a câmera ..."
        set_config_var start_x 1 /boot/config.txt
        CUR_GPU_MEM=$(get_config_var gpu_mem /boot/config.txt)
        
        if [ -z "$CUR_GPU_MEM" ] || [ "$CUR_GPU_MEM" -lt 128 ]; then
            set_config_var gpu_mem 128 /boot/config.txt
        fi

        sed /boot/config.txt -i -e "s/^startx/#startx/"
        sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"
    fi

    log_ok "câmera configurada"
}

function install()
{
    if [ "$UID" != "0" ]; then
        panic "Execute este script como root"
    fi

    # Cria o arquivo de serviço
    create_service

    # Prepara o ambiente de trabalho
    prepare_workspace

    # Instala os pacotes necessários
    install_deps
	
	# Instala os pacotes necessários (python)
	check_python_deps

    # Habilita a câmera
    enable_camera 1

    # Faz a configuração de rede sem fio
    get_wpa_credentials_and_save
}

# Faz a instalação do software
install
