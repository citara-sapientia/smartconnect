/**
 * @file audio.cpp
 * @author Citara Labs Tecnologia
 * @brief Operações com o microfone e o alto-falante
 * @version 0.1
 * @date 2019-10-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include <Arduino.h>
#include "audio.h"
#include "util.h"

static unsigned calibrated_min, calibrated_max;

Audio::Audio()
{

}

Audio::Audio(uint8_t microphone, uint8_t mic_digital_pin)
{
    this->mic_pin = microphone;
    this->mic_pin_digital = mic_digital_pin;
}

Audio::~Audio()
{

}

void Audio::begin()
{  
    // Configura os pinos do módulo de áudio     
    pinMode(this->mic_pin_digital, INPUT);
    pinMode(this->mic_pin, INPUT);

    this->reference_voltage = this->analogToVolts(this->read_microphone());

    // Calibra o microfone
    this->calibrate_audio_device(&::calibrated_min, &::calibrated_max);
}

unsigned Audio::read_microphone()
{
    return static_cast<unsigned>(analogRead(this->mic_pin));
}

unsigned Audio::digital_read_microphone()
{
    return static_cast<unsigned>(digitalRead(this->mic_pin_digital));
}

void Audio::calibrate_audio_device(unsigned *min, unsigned *max)
{
    unsigned current, _min, _max = 0;

    for(int i = 0; i < 5 /*AUDIO_MODULE_NUMBER_OF_READINGS */; i++)
    {
        // Lê o microfone
        current = static_cast<unsigned>(this->read_microphone());

        // Configura o valor de referência
        if(i == 0) 
            _min = current;

        // Atualiza o valor máximo lido
        if(current > _max)
            _max = current;
        
        // Atualiza o valor mínimo lido
        if(current < _min)
            _min = current;

        delay(timeout);
    }

    *min = _min;
    *max = _max;
}

bool Audio::get_audio_data(struct audio_module_values *amv)
{
    unsigned current;
    unsigned max_to_send, min_to_send;
    bool have_two_extreme_values = false;
    unsigned long _delay = 0;

    // Lê o microfone durante 1 segundo (10 leituras ao total)
    for(unsigned i = 0; i < 2 /*AUDIO_ENVIRONMENT_READINGS*/; i++)
    {
        current = this->read_microphone();
        if(current > ::calibrated_max)
            max_to_send = current;
        
        if(current < ::calibrated_min) {
            min_to_send = current;

            // TODO: implementar o envio do delay caso isso ocorra
            have_two_extreme_values = true;

            // Calcula o tempo entre as leituras (para enviar futuramente no payload)
            _delay = timeout * (i+1);
        }

        delay(timeout / 100);
    }
    
    // Coloca no formato pronto para envio
    amv->current = static_cast<unsigned>(convert_to_dB(current) * 100);
    amv->max = static_cast<unsigned>(convert_to_dB(::calibrated_max) * 100);
    amv->min = static_cast<unsigned>(convert_to_dB(::calibrated_min) * 100);

    // Verifica se o valor deve ser enviado ao middleway 
    if(isValueInsideInterval(amv->current, amv->min, amv->max))
        return false;
    
    return true;
}

float Audio::analogToVolts(unsigned analog_value)
{
    float voltage = 0;
    const float max_analog_pin_voltage = 5.0;
    const float max_analog_value = 1023.0;

    voltage = (max_analog_pin_voltage * static_cast<float>(analog_value)) / max_analog_value;

    return voltage;
}

float Audio::convert_to_dB(unsigned analog_value)
{
    return static_cast<float>(20.0 * log10((this->reference_voltage / static_cast<float>(analog_value))));
}
