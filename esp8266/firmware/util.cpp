/**
 * @file util.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-11-14
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "util.h"

vector<string> split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    string token;

    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) 
            pos = str.length();
        
        token = str.substr(prev, pos - prev);

        if (!token.empty()) 
            tokens.push_back(token);

        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());

    return tokens;
}

bool isValueInsideInterval(int value, int min, int max)
{
    return (((value > max) || (value < min)) ? true : false);
}
