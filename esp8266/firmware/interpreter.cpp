/**
 * @file interpreter.cpp
 * @author Citara Labs Tecnologia
 * @brief 
 * @version 0.1
 * @date 2019-10-02
 * 
 * @copyright Copyright (c) Citara Labs 2019
 * 
 */

#include <Arduino.h>
#include "interpreter.h"
#include "temperature.h"
#include "serial_handler.h"
#include "pins.h"
#include "audio.h"
#include "util.h"

using namespace commands;

#define DHT11_PIN           D5
#define MIC_DIGITAL_PIN     D4
#define MIC_ANALOG_PIN      A0

// Flag para saber se o dispositivo já foi calibrado
static bool calibrated = false;

Interpreter::Interpreter() : SerialHandler()
{
    is_running = false;
}

Interpreter::~Interpreter()
{
    // Finaliza a comunicação serial
    this->end();
}

void Interpreter::Run()
{
    ExecuteInterpreter();
}

void Interpreter::ExecuteInterpreter()
{
    String serialData = "";
    TemperatureSensor dht_sensor(DHT11_PIN);
    Audio hw_484(MIC_ANALOG_PIN, MIC_DIGITAL_PIN);
    struct audio_module_values hw_484_data;
    float temperature = 0;
    float humidity = 0;
    int bytes_available;
    bool can_send = false;
    short cmd_id;

    // Lê a porta serial "infinitamente"
    is_running = true;

    // Configura o módulo de som
    hw_484.begin();

    // Configura o módulo meteorológico (DHT11)
    dht_sensor.begin();

    while(!b_stop)
    {
        // Verifica se existe algum dado na porta
        bytes_available = available();

        if(bytes_available > 0)
        {
            serialData = readString();

            if(!(serialData.length() > 0))
                continue;

            // Analisa o comando
            cmd_id = ParseCommand(serialData);

            // Executa a devida ação, de acordo com o comando recebido
            switch(cmd_id)
            {
                case CMD_TEMP:

                    // Lê a temperatura do sensor
                    temperature = dht_sensor.getTemperature();

                    // Valida a temperatura
                    if(isnan(temperature)) {
                        this->SendErrorPayload(ERR_INVALID_TEMP);
                        break;
                    }

                    // Envia a temperatura lida pela porta
                    this->SendTemperature(temperature);
                break;

                case CMD_HUM:

                    // Lê a umidade
                    humidity = dht_sensor.getHumidity();

                    // Valida a umidade
                    if(isnan(humidity)) {
                        SendErrorPayload(ERR_INVALID_HUM);
                        break;
                    }

                    // Envia a umidade lida
                    this->SendHumidity(humidity);
                break;

                case CMD_MIC:

                    // Lê o sensor de áudio
                    can_send = hw_484.get_audio_data(&hw_484_data);

                    // Envia o dado via serial para que ele seja enviado ao middleway
                    if(can_send)
                        this->SendMicValue(&hw_484_data);
                break;

                case CMD_UNKNOWN:
                    // O comando é desconhecido
                    SendErrorPayload(CMD_UNKNOWN);
                break;
            }
        }
    }

    b_stop = false;
}

short Interpreter::ParseCommand(String cmd = "")
{
    unsigned int size = cmd.length();

    // Valida o size e o formato
    if((size == 0) || (cmd.indexOf("+") == -1))
        return CMD_UNKNOWN;

    if(cmd.startsWith(commands::mic_cmd))
        return CMD_MIC;
    else if(cmd.startsWith(commands::humidity_cmd))
        return CMD_HUM;
    else if(cmd.startsWith(commands::temperature_cmd))
        return CMD_TEMP;
    else
        return CMD_UNKNOWN;
}

bool Interpreter::isRunning()
{
    return this->is_running;
}

void Interpreter::stop()
{
    this->b_stop = true;
}

const char *BuildErrorPayload(int error_code = ERR_UNKNOWN)
{
    char buffer[BUFLEN];

    snprintf(buffer, ARRAYLEN(buffer), "%s/%04x", err_cmd, error_code);

    return const_cast<const char*>(&buffer[0]);
}

void Interpreter::SendErrorPayload(int error_code)
{
    const char *error_payload;
    
    error_payload = BuildErrorPayload(error_code);

    this->println(error_payload);
}

void Interpreter::ReportSucessfullyOperation()
{
    SendErrorPayload(0);
}
