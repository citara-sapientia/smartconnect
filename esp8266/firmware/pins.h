/**
 * @file pins.h
 * @author Citara Labs Tecnologia
 * @brief Pinos utilizados pelo firmware
 * @version 0.1
 * @date 2019-10-02
 * 
 * @copyright Copyright (c) Citara Labs 2019
 * 
 */

#ifndef PINS_H
#define PINS_H

#define PIN_BUZZER                  5
#define PIN_MIC                     A0
#define PIN_TEMPERATURE_SENSOR      D5

#endif