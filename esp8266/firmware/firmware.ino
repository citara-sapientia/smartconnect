#include "interpreter.h"
#include <DHT.h>
#include "DHT_U.h"
#include "gsm.h"
#include "audio.h"
#include "temperature.h"

Interpreter *inter = nullptr;
GSM *chip = nullptr;
bool gsm_module_enabled;

void setup()
{
    if(!chip)
        gsm_module_enabled = false;

    // Tenta inicializar o módulo GPRS
    if(gsm_module_enabled)
        gsm_module_enabled = chip->begin();

    // Inicializa o interpretador de comandos
    inter = new Interpreter();
}

void loop()
{   
    // Executa o interpretador
    if(!inter->isRunning())
        inter->Run();
}
