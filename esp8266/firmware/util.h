/**
 * @file util.h
 * @author Citara
 * @brief Funções de propósito geral
 * @version 0.1
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>

using namespace std;

/**
 * @brief Retorna a quantidade de itens no array
 * 
 */
#define ARRAYLEN(x) (sizeof(x)/sizeof(x[0]))

/**
 * @brief Tamanho do buffer interno
 * 
 */
#define BUFLEN  256

/**
 * @brief Quebra uma string em um vetor de string, separadas por um delimitador
 * 
 * @param str String
 * @param delim Delimitador
 * @return vector<string> Vetor com as strings separadas
 */
vector<string> split(const string& str, const string& delim);

/**
 * @brief Verifica se o valor está dentro do intervalo fechado
 * 
 * @param value Valor a ser verificado
 * @param min Mínimo
 * @param max Máximo
 * @return true Se o valor está no intervalo
 * @return false Se não estiver
 */
extern bool isValueInsideInterval(int value, int min, int max);

#endif
