/**
 * @file serial.h
 * @author Lucas Vieira de Jesus (lucas.vieira@citara.io)
 * @brief Comunicação serial entre o raspberry e o arduino
 * @version 0.1
 * @date 2019-07-26
 * 
 * @copyright Citara Labs (c) 2019
 * 
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "audio.h"

/**
 * @brief Comandos da porta serial
 * 
 */
namespace commands
{
    static const char *temperature_cmd = "AT+TEMP";
    static const char *humidity_cmd = "AT+HUM";
    static const char *mic_cmd = "AT+MIC";
    static const char *err_cmd = "AT+ERR";
}

class SerialHandler : public HardwareSerial
{
    public:
        /**
         * @brief Inicializa a instância da classe
         * 
         */
        SerialHandler();

        /**
         * @brief Inicializa a instância da classe
         * 
         * @param baud_rate Taxa de transmissão em bits por segundo
         */
        SerialHandler(int baud_rate);

        /**
         * @brief Destrói a instância da classe
         * 
         */
        ~SerialHandler();

        /**
         * @brief Escreve a temperatura lida pelo sensor na porta serial
         * 
         * @param temp Temperatura
         */
        void SendTemperature(float temp);

        /**
         * @brief Escreve a umidade lida na porta serial
         * 
         * @param humidity Umidade percentual
         */
        void SendHumidity(float humidity);
        
        /**
         * @brief Envia o valor do microfone
         * 
         * @param audio_module_values Dados do microfone
         */
        void SendMicValue(struct audio_module_values *audio_module_values);
};

#endif
