/**
 * @file temperature.h
 * @author Lucas Vieira de Jesus (lucas.vieira@citara.io)
 * @brief Definições de classe
 * @version 0.1
 * @date 2019-07-26
 * 
 * @copyright Citara Labs (c) 2019
 * 
 */

#include <DHT.h>
#include <DHT_U.h>

class TemperatureSensor : public DHT
{
    public:
        /**
         * @brief Cria uma instância da classe
         * 
         * @param data_pin Pino de dados do sensor
         */
        explicit TemperatureSensor(int data_pin);

        /**
         * @brief Destrói a instância da classe
         * 
         */
        ~TemperatureSensor();

        /**
         * @brief Lê a temperatura do sensor
         * 
         * @return float Temperatura em graus Celsius
         */
        float getTemperature();

        /**
         * @brief Lê a umidade do sensor
         * 
         * @return float Umidade percentual
         */
        float getHumidity();
};
