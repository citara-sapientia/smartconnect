/**
 * @file gsm.cpp
 * @author Lucas Vieira de Jesus (lucas.vieira@citara.io)
 * @brief Gerenciamento do módulo de comunicação via rede de dados
 * @version 0.1
 * @date 2019-07-29
 * 
 * @copyright Citara Labs (c) 2019
 * 
 */

#include "gsm.h"
#include "util.h"

GSM::GSM(int rx_pin, int tx_pin) : SoftwareSerial(rx_pin, tx_pin)
{
    // Inicializa as variáveis internas
    cmd = "";
    response = "";
}

GSM::~GSM()
{
    // Finaliza a comunicação serial com o módulo
    end();
}

bool GSM::begin()
{
    const uint16_t timeout = 15000;
    const uint16_t default_delay = 100;
    uint16_t time_spent = 0;

    pinMode(D4, OUTPUT);

    // Inicializa a comunicação serial com o módulo
    SoftwareSerial::begin(this->baud_rate);

    // Reinicia o módulo GPRS
    digitalWrite(D4, LOW);
    delay(1000);
    digitalWrite(D4, HIGH);
    delay(2000);
    digitalWrite(D4, LOW);
    delay(5000);

    // Limpa a porta serial
    SoftwareSerial::flush();
    while(SoftwareSerial::available())
        SoftwareSerial::read();
    
    delay(default_delay * 10);

    // Manda um 'ping' para o módulo e aguarda pela resposta
    while(!SoftwareSerial::available())
    {
        SoftwareSerial::print(String("AT") + 0x0d);
        delay(default_delay);
        time_spent += default_delay;

        if(time_spent >= timeout)
            return false;
    }

    return true;
}

void GSM::turn_off(bool emergency_poweroff)                                         
{
    int mode;

    // Configura o modo
    mode = emergency_poweroff ? 0:1;

    // Formata o comando
    cmd = "AT+CPOWD=" + mode + String("\r\n");

    // Envia o comando para o módulo
    write(cmd.c_str());

    // Limpa a variável que armazena os comandos
    clear_cmd_var();
}

void GSM::clear_cmd_var()
{
    this->cmd = "";
}

const char *GSM::read_cmd_response()
{
    String response = "empty";
    
    // Lê a resposta
    if(available())
        response = readString();

    return response.c_str();
}

String GSM::get_imei()
{
    String imei = "";

    // Formata o comando
    cmd = "AT+GSN\r\n ";

    // Envia o comando
    print(cmd);

    // Limpa a variável que armazena o comando
    clear_cmd_var();

    // Aguarda pela resposta do comando
    wait_for_response(5000);

    // Lê a resposta do comando
    while(available())
        imei += char(read());

    // Retorna o IMEI
    return imei;
}

bool GSM::factory_reset()
{
    // Formata o comando
    cmd = "AT&F0\r\n";

    // Envia o comando
    write(cmd.c_str());
    
    // Limpa a variável que armazena o comando
    clear_cmd_var();

    // Aguarda pela resposta e a lê
    //while(!available());

    return read_cmd_response();
}

const char *GSM::get_configuration()
{
    String configuration = "";

    // Formata o comando
    cmd = "AT+&V\r\n";

    // Envia o comando
    write(cmd.c_str());
    clear_cmd_var();

    // Aguarda pela resposta
    wait_for_response(5000);
    
    // Lê a resposta
    configuration = read_cmd_response();

    return configuration.c_str();
}

void GSM::get_battery_charge(struct battery_charge *charge)
{
    int begin_index;
    String payload = "", data = "";
    vector<string> payload_data;
    int count = 0;

    if(charge == nullptr)
        return;

    // Envia o comando pela porta
    cmd = "AT+CBC";
    write(cmd.c_str());

    // Aguarda pela resposta
    while(!available())
        delay(10);
    
    // Analisa o comando devolvido
    response = read_cmd_response();

    if(!response.startsWith("+CBC:"))
        return;

    // Extrai o payload
    begin_index = response.indexOf(":")+1;
    payload = response.substring(begin_index);
    payload_data = split(payload.c_str(), ",");

    // Preenche a struct com os dados necessários
    for(string data : payload_data)
    {
        switch(count)
        {
            case 0:
                charge->bcs = static_cast<short>(String(data.c_str()).toInt());
            break;

            case 1:
                charge->bcl = static_cast<short>(String(data.c_str()).toInt());
            break;

            case 2:
                charge->voltage = String(data.c_str()).toFloat();
            break;
        }
        count++;
    }
}

bool GSM::delete_sms_message(int index, short del_flag)
{
    String cmd = "AT+CMGD=";
    String response = "";
    int lapsed = 0;
    short timeout = 10;

    // Valida as flags passadas
    switch(del_flag)
    {
        case DEL_ALL_INCLUDE_UNREADED:
        case DEL_ALL_LEAVE_UNREAD:
        case DEL_SPECIFIED_MSG:
        case DEL_ALL_LEAVE_UNTOUCHED:
        case DEL_ALL_READ_MSGS:
        break;

        default:
            return false;
    }

    cmd += index + String(",") + del_flag;

    // Envia o comando
    write(cmd.c_str());

    // Aguarda pela resposta
    wait_for_response(MAX_DEL_RESPONSE_TIME_PER_MSG);
    
    // Lê a resposta
    response = read_cmd_response();
    
    return response == "OK";
}

void GSM::wait_for_response(int timeout)
{
    int lapsed = 0;
    int wait = 10;

    // Aguarda até que algum dado esteja disponível ou o timeout acabe
    while(!available())
    {
        delay(wait);
        lapsed += wait;

        if(lapsed >= timeout)
            break;
    }
}

bool GSM::set_sms_mode(short mode)
{
    String cmd = "AT+CMGF=";
    String response = "";

    // Realiza a validação do modo
    switch(mode)
    {
        case PDU:
        case TEXT:
            break;

        default:
            return false;
    }

    // Adiciona o modo ao comando
    cmd += String(mode);
    
    // Escreve o comando
    write(cmd.c_str());

    // Aguarda pela resposta
    wait_for_response(3000);

    response = read_cmd_response();

    return response == "OK";
}

bool GSM::mem_write_sms_message(struct sms_write_params *msg)
{
    String cmd = "AT+CMGW=";
    String response = "";

    if(msg == nullptr)
        return false;

    cmd += String(msg->origin_addr) + "/";
    cmd += String(msg->dest_addr) + ",";
    cmd += String(msg->type_origin_addr) + "/";
    cmd += String(msg->type_destiny_addr) + ",";
    cmd += String(msg->stat) + 0xD;
    cmd += msg->message;

    // Escreve a mensagem na memória
    write(cmd.c_str());

    // Aguarda pela resposta
    wait_for_response(MAX_SAVE_MSG_RESPONSE_TIME);

    // Lê a resposta
    response = read_cmd_response();
    
    return response.startsWith("+CMGW:");
}

bool GSM::send_sms_message(unsigned short index, const char *dest_addr, short dest_addr_type)
{
    String cmd = "AT+CMSS=";
    String response = "";

    cmd += String(index) + ",";
    cmd += String(dest_addr) + ",";
    cmd += String(dest_addr_type);

    // Envia o comando
    if(!write(cmd.c_str()))
        return false;

    // Aguarda pela resposta
    wait_for_response(MAX_SMS_SEND_TIMEOUT);

    // Lê a resposta
    response = read_cmd_response();

    return response.startsWith("+CMSS:"); 
}