/**
 * @file serial.cpp
 * @author Lucas Vieira de Jesus (lucas.vieira@citara.io)
 * @brief Comunicação serial com o Raspberry Pi
 * @version 0.1
 * @date 2019-07-26
 * 
 * @copyright Citara Labs (c) 2019
 * 
 */

#include <Arduino.h>
#include "serial_handler.h"
#include "util.h"

using namespace commands;

SerialHandler::SerialHandler() : HardwareSerial(0)
{
    unsigned long baud_rate = 9600;

    // Inicializa a comunicação serial
    begin(baud_rate);
}

SerialHandler::SerialHandler(int baud_rate = 9600) : HardwareSerial(0)
{
    // Inicializa a comunicação serial
    begin(static_cast<unsigned long>(baud_rate));
}

SerialHandler::~SerialHandler()
{
    // Finaliza a comunicação serial
    end();
}

void SerialHandler::SendTemperature(float temperature)
{
    char serial_payload[BUFLEN];

    // Formata o payload
    snprintf(serial_payload, ARRAYLEN(serial_payload), "%s/%08x", temperature_cmd, static_cast<uint32_t>(temperature * 1000));

    // Envia pela porta serial
    this->println(serial_payload); 
}

void SerialHandler::SendHumidity(float humidity)
{
    char serial_payload[BUFLEN];

    // Formata o payload
    snprintf(serial_payload, ARRAYLEN(serial_payload), "%s/%08x", humidity_cmd, static_cast<uint32_t>(humidity * 1000));
    
    // Envia pela porta serial
    this->println(serial_payload);
}

void SerialHandler::SendMicValue(struct audio_module_values *sensor_data)
{
    char serial_payload[BUFLEN];

    // Formata o payload
    snprintf(serial_payload, ARRAYLEN(serial_payload), "%s/%08x%08x%08x", mic_cmd, sensor_data->current, sensor_data->min, 
    sensor_data->max);

    // Envia pela porta serial
    this->println(serial_payload);
}
