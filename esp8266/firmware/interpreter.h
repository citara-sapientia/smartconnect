/**
 * @file interpreter.h
 * @author Citara Labs Tecnologia
 * @brief 
 * @version 0.1
 * @date 2019-10-02
 * 
 * @copyright Copyright (c) Citara Labs 2019
 * 
 */

/*
    Interpretador de comandos da porta serial
*/

#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "serial_handler.h"

/**
 * @brief ID dos comandos
 * 
 */
enum Commands_Ids
{
    CMD_TEMP = 0x7A, // Temperatura
    CMD_HUM,  // Umidade
    CMD_MIC,  // Microfone
    CMD_UNKNOWN // Desconhecido
};

/**
 * @brief Códigos de erro
 * 
 */
enum Errors
{
    ERR_INVALID_TEMP = 0x8A,
    ERR_INVALID_HUM,
    ERR_UNKNOWN
};

class Interpreter : public SerialHandler
{
    public:
        /**
         * @brief Construct a new Interpreter object
         * 
         */
        Interpreter();

        /**
         * @brief Destroy the Interpreter object
         * 
         */
        ~Interpreter();

        /**
         * @brief Executa o interpretador de comandos
         * 
         */
        void Run();

        /**
         * @brief Determina se o interpreter está rodando
         * 
         * @return true Se estiver em execução
         * @return false Caso contrário
         */
        bool isRunning();

        /**
         * @brief Finaliza o loop do interpreter
         * 
         */
        void stop();

    private:
        /**
         * @brief Executa o interpretador de comandos
         * 
         */
        void ExecuteInterpreter();

        /**
         * @brief Determina qual comando foi recebido
         * 
         * @param cmd Comando
         * @return short ID do comando
         */
        short ParseCommand(String cmd);

        /**
         * @brief Envia o payload de erro
         * 
         * @param error_code Código do erro (Hexadecimal)
         */
        void SendErrorPayload(int error_code);

        /**
         * @brief Envia um payload para a unidade de comando (raspberry)
         * reportando o sucesso de uma operação.
         * 
         */
        void ReportSucessfullyOperation();

    private:
        // Determina se o interpretador está em execução
        bool is_running;
        bool b_stop;
};

#endif