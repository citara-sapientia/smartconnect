#include <DHT.h>
#include "temperature.h"
#include "Arduino.h"


TemperatureSensor::TemperatureSensor(int pin) : DHT(pin, DHT11)
{
    // Construtora
    begin();
}

TemperatureSensor::~TemperatureSensor()
{
    // Destrutora
}

float TemperatureSensor::getTemperature()
{
    float temp = 0;

    // Lê a temperatura do sensor
    temp = readTemperature();

    return temp;
}

float TemperatureSensor::getHumidity()
{
    float humidity = 0;
    
    // Lê a umidade do sensor
    humidity = readHumidity();

    return humidity;
}