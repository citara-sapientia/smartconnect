/**
 * @file audio.h
 * @author Citara Labs Tecnologia
 * @brief Operações com o microfone e o alto-falante (buzzer)
 * @version 0.1
 * @date 2019-10-03
 * 
 * @copyright Copyright (c) Citara Labs 2019
 * 
 */

#ifndef AUDIO_H
#define AUDIO_H

#include <string.h>

using namespace std;

/**
 * @brief Numero total de leituras para calibrar o dispositivo
 * 
 */
#define AUDIO_MODULE_NUMBER_OF_READINGS 120

/**
 * @brief Número de leituras para registrar os dados do ambiente
 * 
 */
#define AUDIO_ENVIRONMENT_READINGS 10

/**
 * @brief Tamanho do payload de áudio
 * 
 */
#define AUDIO_PAYLOAD_LEN   12

typedef struct audio_module_values
{
    unsigned min;
    unsigned max;
    unsigned current;
} AUDIO_MODULE_VALUES;

/**
 * @brief Gerenciamento dos auto-falantes e do microfone
 * 
 */
class Audio
{
    public:
        /**
         * @brief Constrói o objeto Audio
         * 
         */
        Audio();

        /**
         * @brief Construct a new Audio object
         * 
         * @param microphone 
         * @param mic_digital_pin 
         */
        Audio(uint8_t microphone, uint8_t mic_digital_pin);

        /**
         * @brief Configura o módulo de áudio
         * 
         */
        void begin();

        /**
         * @brief Destrói o objeto Audio
         * 
         */
        ~Audio();

        /**
         * @brief Lê o valor analógico captado pelo microfone
         * 
         * @return unsigned 
         */
        unsigned read_microphone();

        /**
         * @brief Lê o pino digital do microfone
         * 
         * @return unsigned 
         */
        unsigned digital_read_microphone();
        
        /**
         * @brief Realiza a leitura do sensor de áudio
         * 
         * @param amv Ponteiro para a estrutura que armazena os valores lidos
         * @return true Se o dado deve ser enviado ao middleway
         * @return false Caso contrário
         */
        bool get_audio_data(struct audio_module_values *amv);

        /**
         * @brief 
         * 
         * @param min 
         * @param max 
         */
        void calibrate_audio_device(unsigned *min, unsigned *max);
        
        /**
         * @brief Converte o valor analógico para uma tensão em Volts
         * 
         * @param analog_value 
         * @return float 
         */
        float analogToVolts(unsigned analog_value);

        /**
         * @brief 
         * 
         * @param analog_value 
         * @return float 
         */
        float convert_to_dB(unsigned analog_value);

        const char *send_audio_payload();

    private:

        /**
         * @brief Pino do microfone
         * 
         */
        uint8_t mic_pin;

        /**
         * @brief Pino digital do microfone
         * 
         */
        uint8_t mic_pin_digital;
        
        /**
         * @brief Tensão de referência
         * 
         */
        float reference_voltage;

        /**
         * @brief Tempo entre as leituras do sensor de áudio
         * 
         */
        const unsigned long timeout = 1000;
        
};

#endif
