/**
 * @file gsm.h
 * @author Lucas Vieira de Jesus (lucas.vieira@citara.io)
 * @brief Gerenciamento do módulo de comunicação via rede de dados
 * @version 0.1
 * @date 2019-07-29
 * 
 * @copyright Citara Labs (c) 2019
 * 
 */

#include <Arduino.h>
#include <SoftwareSerial.h>

// Tempo máximo de resposta para deletar uma mensagem (milissegundos)
#define MAX_DEL_RESPONSE_TIME_PER_MSG   5000
#define MAX_SAVE_MSG_RESPONSE_TIME      5000
#define MAX_SMS_SEND_TIMEOUT            60000

enum sms_modes 
{
    // Modo PDU (binário)
    PDU, 

    // Modo texto
    TEXT 
};

enum sms_delflags
{
    /*
        Delete the message specified in <index>
    */
    DEL_SPECIFIED_MSG,

    /* 
        Delete all read messages from preferred message storage, leaving unread messages 
        and stored mobile originated messages (whether sent or not) untouched
    */
    DEL_ALL_READ_MSGS,

    /*
        Delete all read messages from preferred message storage and sent mobile originated 
        messages, leaving unread messages and unsent mobile originated messages untouched
    */
    DEL_ALL_LEAVE_UNREAD,

    /*
        Delete all read messages from preferred message storage, sent and unsent mobile 
        originated messages leaving unread messages untouched
    */
    DEL_ALL_LEAVE_UNTOUCHED,

    /*
        Delete all messages from preferred message storage including unread messages
    */
    DEL_ALL_INCLUDE_UNREADED
};

/**
 * @brief 
 * 
 */
typedef struct battery_charge
{
    short bcs; // status de carga
    short bcl; // nível de conexão
    float voltage; // tensão (mV)
} BATTERY_CHARGE;

/**
 * @brief 
 * 
 */
typedef struct sms_write_params
{
    const char *origin_addr;
    const char *dest_addr;
    const char *message;
    unsigned short type_origin_addr;
    unsigned short type_destiny_addr;
    short stat;
} SMS_WRITE_PARAMS;

class GSM : public SoftwareSerial
{
    public:
        /**
         * @brief Constrói o objeto GSM
         * 
         * @param rx_pin Número do pino RX
         * @param tx_pin Número do pino TX
         */
        GSM(int rx_pin, int tx_pin);

        /**
         * @brief Destrói o objeto GSM
         * 
         */
        ~GSM();

        /**
         * @brief Inicializa a comunicação serial
         * 
         * @return true Se a comunicação for inicializada corretamente
         * @return false Se a comunicação não for inicializada
         */
        bool begin();

        /**
         * @brief Lê a configuração atual do módulo
         * 
         * @return const char* String contendo detalhes da configuração
         */
        const char *get_configuration();

        /**
         * @brief Reseta o módulo para as configurações de fábrica
         * 
         * @return true Em caso de sucesso
         * @return false Em caso de erro
         */
        bool factory_reset();

        /**
         * @brief Obtém o IMEI do módulo
         * 
         * @return String IMEI lido
         */
        String get_imei();

        /**
         * @brief Desliga o módulo GSM
         * 
         * @param emergency_poweroff true se o desligamento for de emergência
         */
        void turn_off(bool emergency_poweroff);

        /**
         * @brief Obtém a carga da bateria
         * 
         * @param charge Estrutura que recebe as informações da bateria
         */
        void get_battery_charge(struct battery_charge *charge);
        
        /**
         * @brief Deleta uma mensagem de SMS da memória
         * 
         * @param index Posição na memória
         * @param del_flag Flag de deleção
         * @return true Se a mensagem for deletada
         * @return false Em caso de erro
         */
        bool delete_sms_message(int index, short del_flag);

        /**
         * @brief Configura o modo de mensagem SMS
         * 
         * @param mode pode ser PDU ou TEXT
         * @return true Se a operação for bem sucedida
         * @return false Se der erro
         */
        bool set_sms_mode(short mode);

        /**
         * @brief Salva uma mensagem de sms na memória
         * 
         * @param msg 
         * @return true 
         * @return false 
         */
        bool mem_write_sms_message(struct sms_write_params *msg);  

        /**
         * @brief Envia uma mensagem de SMS armazenada na memória
         * 
         * @param index Posição da mensagem na memória
         * @param dest_addr Endereço de destino
         * @param dest_addr_type Tipo do endereço de destino
         * @return true Se a mensagem for enviada
         * @return false Em caso de erro
         */
        bool send_sms_message(unsigned short index, const char *dest_addr, short dest_addr_type);

    private:
        /**
         * @brief Taxa de transmissão
         * 
         */
        const int baud_rate = 9600;

        /**
         * @brief String reservada para armazenar os comandos a enviar via porta serial
         * 
         */
        String cmd;

        /**
         * @brief String reservada para armazenar as respostas dos comandos enviados ao módulo
         * 
         */
        String response;

    private:
        /**
         * @brief Limpa o comando do buffer
         * 
         */
        void clear_cmd_var();

        /**
         * @brief Lê a resposta do comando recebido
         * 
         * @return const char* String com a resposta recebida
         */
        const char *read_cmd_response();

        /**
         * @brief Aguarda por uma resposta na porta serial
         * 
         * @param timeout Tempo máximo de espera (em milissegundos)
         */
        void wait_for_response(int timeout);
};