# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Cliente HTTP do middleway
#

from m_tcp import MiddlewayTcpClient
from base64 import b64encode
from camera import Camera, ImageFormats
from logger import log_info, log_error, log_ok
from time import sleep, time
import time
import asyncio
from m_uuid import uuid
import os
import constants
import database


class MiddlewayHttpClient(MiddlewayTcpClient):

    # Porta de conexão
    http_port = 8082

    # Tamanho do buffer para receber os dados
    chunk_size = 512

    # Timeout padrão (em segundos)
    #timeout = 5

    def __init__(self):
        MiddlewayTcpClient.__init__(self)

    # Envia request de autenticação
    def http_send_auth_request(self, scenario_id=""):
        """Envia uma request de autenticação

        Keyword Arguments:
            scenario_id {str} -- [id do cenário] (default: {""})

        Returns:
            [str] -- [origin_id (Token de autenticação)]
        """

        # Valida o scenario_id
        if uuid(scenario_id).parse() is False:
            log_error(f"ID inválido: {scenario_id}")
            return ""

        # Resposta do servidor
        full_server_response = ""

        # Id do objeto
        origin_id = ""

        # Se o id já existe no banco, então devemos pegá-lo e retornar
        db = database.Database()

        # Tenta ler o id do banco
        origin_id = db.get_origin_id(scenario_id)
        
        if origin_id == "":

            # Realiza a autenticação    
            request = ""

            # Conecta-se com o middleway
            socket = self.connect(self.http_port)

            # Formata a requisição
            request = f"POST {constants.auth_string} HTTP/1.1\r\n"
            request += f"Host: 0.0.0.0:{self.http_port}\r\n"
            request += f"Coi: {scenario_id}\r\n\r\n"

            # Escreve a requisição no canal
            socket.sendall(bytes(request, self.default_encoding))

            # Lê a resposta do server em partes de 512 bytes
            http_response = socket.recv(self.chunk_size).decode(self.default_encoding)

            while len(http_response) > 0:
                full_server_response += http_response
                http_response = socket.recv(self.chunk_size).decode(self.default_encoding)

            # Finaliza a conexão
            socket.close()

            # Número de vezes que os caracteres '\r' ou '\n' foram localizados
            lines = 0

			# Inicializa a variável pra guardar o id
            origin_id = ""

            # Obtém o token devolvido pelo server
            for item in full_server_response:
                if item is '\r' or item is '\n':
                    lines += 1
                else:
                    if lines == 4:
                        origin_id += item
                    else:
                        lines = 0

            # Salva o token no banco
            if db.save_origin_id(scenario_id, origin_id) is False:
                log_error("Falha ao salvar o token no banco")
            else:
                return origin_id
        else:
            return origin_id

    def http_send(self, scenario_id="", payload=b"", is_media_scenery=False):

        # Realiza a conexão
        http_connection = self.connect(self.http_port)

        if http_connection is None:
            log_error("[http-client] falha ao se conectar com o middleway")
            return False

        # Configura o timeout
        #http_connection.settimeout(self.timeout)

        # Obtém o token
        token = self.http_send_auth_request(scenario_id)

        if token == "":
            log_error("[http-client] id inválido")
            return False

        # Constrói a request
        request = self.build_http_request(token, payload, constants.send_string, is_media_scenery)

        # Envia para o middleway
        http_connection.sendall(bytearray(request, self.default_encoding))

        # Fecha a conexão
        http_connection.close()

        return True

    def build_http_request(self, scenario_id="", payload=b"", request="send", media_scenery=False):
        s_payload = payload.decode(self.default_encoding)

        header = f"POST {request} HTTP/1.1\r\n"
        header += f"Content-Length: {len(payload)}\r\n"
        #header += f"Content-Type: text/html\r\n"
        header += f"Host: 0.0.0.0:{self.http_port}\r\n"
        header += f"Coi: {scenario_id}\r\n\r\n"

        # Se o cenário de mídia for o alvo, então o payload já estará codificado em base64
        for item in payload:
            s_payload += str(item)

        return header + s_payload

    # Envia uma imagem para o middleway (cenário de mídia)
    def http_send_image(self, image_name="", scenario_id=""):
        """Envia uma imagem para o middleway

        Keyword Arguments:
            image_name {str} -- [Nome do arquivo de imagem] (default: {""})
            scenario_id {str} -- [ID do cenário] (default: {""})

        Returns:
            [bool] -- [True: ok, False: erro]
        """

        # Valida os parâmetros
        if image_name == "" or scenario_id == "":
            log_error("image_name -> %s, scenario_id -> %s" % (image_name, scenario_id))
            return False

        # Obtém o ID da imagem
        image_type = Camera.get_image_id_from_filename(image_name)

        # Guarda os dados da imagem
        data = b""

        # Abre o arquivo de imagem
        with open(str(image_name), "rb") as image:
            # Lê a imagem por completo
            data = image.read()

        # Escreve o tipo da imagem no começo do payload
        _type = bytes(str(image_type), self.default_encoding)

        # Codifica para base64
        try:
            encoded = b64encode(data)
        except Exception as err:
            log_error("b64encode(): " + str(err))
            return False

        # Remove os caracteres indesejados
        encoded = _type + encoded

        #log_info("Enviando imagem %s (%d bytes) ..." % (image_name, len(encoded)))

        return self.http_send(scenario_id, encoded, True)

    @staticmethod
    def http_send_image_task(scenario_id="", timeout_sec=1):
        """Task para enviar as imagens capturadas para a nuvem (Middleway)

        Keyword Arguments:
            scenario_id {str} -- [id do cenário]
        """

        # Tempo de espera
        #delay = 1

        # Inicializa a classe da câmera
        camera = Camera()

        # Inicializa o cliente HTTP (as imagens capturadas são enviadas p/ o middleway)
        http_client = MiddlewayHttpClient()

        while constants.cleanup_flag is False:

            # Captura uma imagem da câmera
            #s = time.time()
            filename = camera.capture_image()
            #e = time.time()

            #log_info(f"Time to capture: {e - s}")

             # Vai para a próxima imagem
            if filename == "":
                continue

            # Caminho completo da imagem
            full_path = f"{constants.images_folder}/{filename}"

            # Envia a imagem para o middleway
            try:
                #s = time.time()
                http_client.http_send_image(full_path, scenario_id)

                # Aguarda 10 segundos
                sleep(10)
                #e = time.time()
            except Exception as err:
                log_error("Erro ao enviar imagem: %s" % str(err))

            #log_info(f"Time to send: {e - s}")
            
        log_info("Câmera: finalizando ...")
