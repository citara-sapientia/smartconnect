#!/usr/bin/python
#
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Cliente TCP do middleway
#

from socket import *
from m_uuid import uuid
from logger import log_info, log_error, log_ok
from time import sleep
from base64 import b64encode
from camera import Camera
from camera import ImageFormats
from gpio import Gpio
import pins
import constants
import json
import asyncio
import database


class MiddlewayTcpClient:
    """Simples cliente TCP do Middleway
    """

    # Nome de rede do middleway
    middleway_hostname = "core-dev.citara.io"

    # Endereço de IP do middleway
    ip_address = "127.0.0.1"

    # Porta de conexão
    port = 8083

    # Tipo de codificação a ser usada
    default_encoding = 'utf-8'

    # Timeout padrão (segundos)
    default_timeout = 10

    current_load_state = ""

    # Função construtura da classe
    def __init__(self):
        """Inicializa a classe
        """

        # Resolve o endereço de ip do middleway
        self.ip_address = gethostbyname(self.middleway_hostname)

    # Realiza conexão com o middleway e retorna um socket
    def connect(self, _port=port):
        """Realiza uma conexão TCP com o middleway

        Returns:
            [socket] -- [socket da conexão, None em caso de erro]
        """

        # Cria um socket TCP
        try:
            sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)

            # Realiza a conexão com middleway
            sock.connect((self.ip_address, _port))

            # Configura o timeout
            sock.settimeout(self.default_timeout)

        except Exception as err:
            log_error("connect() with {0}:{1}: {2}".format(self.ip_address, _port, str(err)))
            return None

        return sock
        
	# Envia request de autenticação
    def send_auth_request(self, scenario_id=""):
        """Envia uma request de autenticação
        
        Keyword Arguments:
            scenario_id {str} -- UUID do cenário (default: {""})
        
        Returns:
            [str] -- origin id do cenário
        """

        # Token de autenticação (origin_id)
        token = ""

        # String que armazena a request de autenticação
        request = ""
    
        # Valida o ID fornecido
        if uuid(scenario_id).parse() is False:
            return ""

        # Obtém um objeto para o banco de dados
        db = database.Database()

        # Verifica se já existe um token cadastrado no banco de dados
        token = db.get_origin_id(scenario_id)
        if token != "":
            return token

        # Conecta-se com o middleway
        socket = self.connect()

        # Formata a requisição
        request = f"{constants.auth_string}{scenario_id}"

        # Escreve a requisição no canal
        socket.send(bytearray(request, 'utf-8'))

        # Recebe o id e decodifica-o
        token = socket.recv(uuid.FULL_ID_SIZE).decode(self.default_encoding)

        # Valida o ID recebido
        if uuid(token).parse() is False:
            return ""

        # Salva o ID no banco
        db.save_origin_id(scenario_id, token)

        # Finaliza a conexão
        socket.close()

        # Retorna o token recebido
        return token

    def send(self, scenario_id="", payload=b"0", media_payload=False, image_type=ImageFormats.JPEG):
        """Envia um payload para o middleway
        
        Argumentos:
            scenario_id {str} -- [id de destino] (default: {""})
            payload {bytes} -- [payload a ser enviado] (default: {b"0"})
            media_payload {bool} -- [True: é um payload de mídia, False: caso contrário] (default: {False})
            image_type {int} -- [Tipo da imagem (Ver ImageFormats)] (default: {ImageFormats.JPEG})
        
        Retorno:
            [bool] -- [True: sucesso, False: erro]
        """

        # String para armazenar a request
        request = ""

        # Conecta-se com o middleway
        log_info("Conectando-se com o middleway ...")

        sock = self.connect()

        # Constrói o payload
        if media_payload == False:
            request = f"{constants.send_string}{scenario_id}{str(payload)}"
        else:
            request = f"{constants.send_string}{scenario_id}{str(image_type)}"

        log_info("Enviando payload ...")

        # Constrói a request por completo
        b_request = bytearray(request, self.default_encoding)
        b_payload = bytearray(payload, self.default_encoding)
        b_full = b_request + b_payload

        # Decodifica para remover os caracteres indesejados
        decoded = b_full.decode("utf-8")

        # Escreve a request no canal
        try:
            if (sock.sendall(bytearray(decoded, self.default_encoding))) == None:
                log_ok("Dados enviados com sucesso")
            else:
                log_error("Falha ao enviar dados")
                return False
        except Exception as err:
            log_error("Erro ao enviar imagem: " + str(err))
            
        # Encerra a conexão
        sock.close()
        
        log_ok("Conexão finalizada")

        return True

    def tcp_send_auth(self, scenario_id=""):
        """Envia autenticação ao middleway
        
        Keyword Arguments:
            scenario_id {str} -- id do cenário (default: {""})
        
        Returns:
            [str] -- id devolvido na autenticação
        """

        # Valida o ID
        if uuid(scenario_id).parse() is False:
            return ""

        # Obtém um objeto para realizar operações no banco
        db = database.Database()

        # Verifica se o id já existe no banco. Se existe, então basta retorná-lo
        token = db.get_origin_id(scenario_id)
        if token != "":
            return token

        # Conecta-se ao middleway
        try:
            sock = self.connect()
        except Exception as err:
            log_error(f"Failed to send heartbeat: {str(err)}")
            return ""
        
        # Constrói a request
        request = f"{constants.auth_string}{scenario_id}"
        
        # Envia a request
        sock.send(bytearray(request, 'utf-8'))

        # Lê a conexão e decodifica a resposta
        token = sock.recv(uuid.FULL_ID_SIZE).decode(self.default_encoding)

        # Valida o id recebido
        if uuid(token).parse() is False:
            return ""

        # Salva o origin ID no banco de dados
        db.save_origin_id(scenario_id, token)
        
        # Fecha a conexão
        sock.close()

        return token

    def send_heartbeat(self, scenario_id=""):
        """Envia heartbeat ao middleway
        
        Keyword Arguments:
            scenario_id {str} -- ID do cenário (default: {""})
        
        Returns:
            [str] -- estado atual da carga acionada 
        """

        token = ""
        
        # Valida o ID
        if uuid(scenario_id).parse() is False:
            return ""

        # Cria um objeto para gerenciar o banco
        db = database.Database()

        # Envia a autenticação, se necessário
        token = db.get_origin_id(scenario_id)
        if token == "":

            # Envia a autenticação
            token = self.tcp_send_auth(scenario_id)

            # Valida o token recebido
            if uuid(token).parse() is False:
                return ""

        # Conecta-se ao middleway
        try:
            sock = self.connect()
        except Exception as err:
            log_error(f"Failed to connect: {str(err)}")
            return ""

        # Envia o heartbeat
        request = f"{constants.beat_string}{token}"
        sock.sendall(bytearray(request, 'utf-8'))

        # Lê a resposta
        hb_response = sock.recv(constants.ID_LEN).decode('utf-8')

        key = ""
        state = ""

        try:
            # Carrega o JSON na memória
            key = json.loads(hb_response)

            # Obtém o valor da chave
            state = str(key["power"]).strip()

            if state != "on" and state != "off":
                log_error("Invalid json")
                return ""

        except json.decoder.JSONDecodeError as err:
            log_error(f"Failed to decode JSON: {str(err)}")

            # Fecha o socket
            sock.close()

            return ""

        # Realiza a atuação
        if state == "on":
            Gpio.digitalWrite(pins.RELAY_PIN, 0)
        else:
            Gpio.digitalWrite(pins.RELAY_PIN, 1)

        # Fecha a conexão
        sock.close()

        return state

    @staticmethod
    def change_device_state(scenario_id="", unused=""):
        
        # Inicializa um cliente TCP
        client = MiddlewayTcpClient()

        log_info(f"Sending heartbeat to scenario id {scenario_id}...")

        while constants.cleanup_flag is False:

            # Envia o heartbeat
            try:
                client.send_heartbeat(scenario_id)
            except Exception as err:
                log_error(f"Failed to send heartbeat: {err}")

            sleep(1)

        log_ok("Envio de heartbeat: finalizado")

    # Envia uma imagem para o middleway (cenário de mídia)
    def send_image(self, image_name="", scenery_id=""):
        """Envia uma imagem para o middleway
        
        Argumentos:
            image_name {str} -- [Nome do arquivo de imagem] (default: {""})
            scenery_id {str} -- [ID do cenário] (default: {""})
        
        Retorno:
            [bool] -- [True: sucesso, False: erro]
        """

        # Tipo de imagem (jpeg é o padrão)
        image_type = "jpg"

        # Valida os parâmetros
        if image_name == "" or scenery_id == "":
            return False
        
        log_info("Obtendo o ID da imagem: " + image_name)

        # Obtém o ID da imagem
        image_type = Camera.get_image_id_from_filename(image_name)

        log_info("Lendo o arquivo de imagem ...")
        
        data = b"0"

        # Abre o arquivo de imagem
        with open(str(image_name), "rb") as image:
            # Lê a imagem por completo
            data = image.read()
        
        # Certifica-se de que o arquivo está fechado
        if not image.closed:
            image.close()
        
        # Codifica para base64
        try:
            encoded = b64encode(data)
        except Exception as err:
            log_error("b64encode(): " + str(err))
            return False

        # Remove os caracteres indesejados
        encoded = encoded.decode("utf-8")
        
        log_info("Enviando imagem %s (%d bytes) ..." % (image_name, len(encoded)))
        
        return self.send(scenery_id, str(encoded), True, image_type)
