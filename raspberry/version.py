# -*- coding: utf-8 -*-
#
# Método para obter versão do software
#

from autorevision import VCS_NUM


def get_version():
    """Pega a versao do programa

    Returns:
        [str] -- [versão do programa]
    """

    version = ""
    s_num = str(VCS_NUM / 100)
    dot_count = 0

    # Analisa a string e converte para uma versão
    for v in s_num:
        if v == ".":
            continue

        version += v
        if dot_count < 2:
            version += "."

        dot_count += 1

    return version

if __name__ == "__main__":
    quit()
