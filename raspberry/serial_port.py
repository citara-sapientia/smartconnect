#!/usr/bin/python
#
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Comunicação serial


from time import sleep
from error_parser import error_parser
from logger import log_error, log_info, panic, log_ok
import os
from m_http import MiddlewayHttpClient
import constants

# Sem a comunicação serial, não é possível ler os sensores
try:
    from serial import Serial
    from serial.tools import list_ports
except ImportError as err:
    panic(f"Failed to import serial modules: {str(err)}")

class ModuleType:

    TEMPERATURE = 1
    HUMIDITY = 2
    MICROPHONE = 4


class SerialPort:

    # Nome da porta
    serial_port_name = '/dev/ttyS0'

    # Flag para indicar se a porta serial não existe
    serial_disabled = False

    # Taxa de transmissão
    baud_rate = 9600

    # Identificador para a porta serial
    port = Serial()

    def __init__(self, port_name=serial_port_name):
        """Inicializa a classe serial

        Keyword Arguments:
            port_name {str} -- [Nome da porta serial] (default: {serial_port_name})
        """

        if not port_name == "":
            self.serial_port_name = port_name

        # Verifica se a porta serial em questão existe
        if os.path.exists(self.serial_port_name) is False:
            log_error("The port {0} don't exists".format(self.serial_port_name))
            self.serial_port_name = "/dev/ttyUSB1"

        if not os.path.exists(self.serial_port_name):
            log_error("The port {0} doesn't exists".format(self.serial_port_name))
            self.serial_disabled = True

        log_info(f"Iniciando comunicação serial: {self.serial_port_name} @ {self.baud_rate} bps")

        # Inicializa a classe serial
        try:
            self.port = Serial(self.serial_port_name, self.baud_rate, timeout=0.5)
        except Exception as err:
            log_error(f"Falha ao inicializar classe serial: {str(err)}")
        
        log_ok(f"Comunicação serial configurada com sucesso")

        self.port.flush()

    def open_port(self):
        """Abre a porta serial
        """

        try:
            # Tenta abrir a porta serial
            self.port.open()
            
            # Log
            log_ok(f"Comunicação serial iniciada: {self.serial_port_name} @ {self.baud_rate} bps")
            
        except Exception as err:
            log_error(str(err))

        return True

    # Fecha a porta serial
    def close_port(self):
        """Fecha a porta serial
        """

        # Fecha a porta serial
        self.port.close()

    # Lê uma linha da porta serial
    def read_line(self):
        """Lê uma linha da porta serial

        Returns:
            [str] -- Dados que foram lidos. Retorna uma string vazia em caso de erro
        """

        data = b''

        # Lê a porta serial, linha por linha
        while True:
            s = self.read()
            if s == b'\n':
                break

            data += s

        line = ""

        try:
            line = data.decode('utf-8')
        except UnicodeDecodeError as unicode:
            log_error(f"Can't decode to unicode {str(unicode)}")
            log_info("Threating as ASCII")
        finally:
            try:
                line = data.decode('ascii')
            except Exception as err:
                log_error("Falha ao decodificar")

        return line

    # Lê um byte da porta serial
    def read(self):
        """Lê um byte da porta serial

        Returns:
            [byte] -- [Byte lido]
        """
        
        return self.port.read()

    def write_data(self, data=""):
        """Escreve um dado na porta serial

        Arguments:
            data {str} -- [Dado a ser escrito]
        """

        written = 0
        payload = data.encode('utf-8')
        payload += str("\n").encode("utf-8")

        log_info(f"Request sent over serial: {payload.decode('utf-8')}")

        try:
            written = self.port.write(payload)
        except Exception as err:
            log_error(f"write_data(): {str(err)}")

        # Retorna o número de bytes escritos
        return written

    def read_module_data(self, type=ModuleType.TEMPERATURE):
        """Lê a temperatura, umidade ou sinal de áudio

        Keyword Arguments:
            type {int} -- [Tipo do dado a ser extraído] (default: {ModuleType.TEMPERATURE})

        Raises:
            IOError: [Caso nenhum dado esteja disponível]

        Returns:
            [str] -- Dado recebido pela porta serial
        """

        data = ""

        # Envia a request de leitura
        if type is ModuleType.TEMPERATURE:
            self.write_data(constants.read_temperature_command)
        elif type is ModuleType.HUMIDITY:
            self.write_data(constants.read_humidity_command)
        elif type is ModuleType.MICROPHONE:
            self.write_data(constants.read_microphone_command)
        else:
            log_error("Falha ao ler sensor: módulo desconhecido")
            return ""
        
        log_info("Aguardando sensor ...")
        
        sleep(2)

        # Aguarda pela resposta (faz a checagem a cada 100ms)
        try:
            while self.port.in_waiting <= 0:
                sleep(0.1)
        except Exception as err:
            log_error("Erro ao aguardar resposta: " + str(err))
            return ""

        # Lê o dado da porta
        payload = str(self.read_line())

        if payload == "":
            raise IOError("Nenhum dado está disponível na porta")
        
        if payload.startswith(constants.error_command):
            log_error("Erro: " + str(payload))
            return ""
        
        log_info(f"Serial response: {payload}")

        # Analisa o dado
        if type is ModuleType.HUMIDITY:

            if payload.startswith(constants.read_humidity_command):

                # Separa o dado e obtém a umidade
                try:
                    data = payload.split("/")[1]
                except IndexError as index:
                    data = ""
                    log_error(f"falha ao decodificar a umidade: {str(index)}")

        elif type is ModuleType.TEMPERATURE:
            if payload.startswith(constants.read_temperature_command):

                # Separa o dado e obtém a temperatura
                try:
                    data = payload.split("/")[1]
                except IndexError as index:
                    data = ""
                    log_error(f"falha ao decodificar temperatura: {str(index)}")
                    log_info(f"payload: {payload}")

        elif type is ModuleType.MICROPHONE:

            if payload.startswith(constants.read_microphone_command):

                # Separa o dado
                try:
                    data = payload.split("/")[1]
                except IndexError as index:
                    data = ""
                    log_error(f"falha ao decodificar dados de áudio: {str(index)}")

        else:
            data = ""
            print("Módulo desconhecido ou não conectado ao core")

        return data

if __name__ == "__main__":
    quit()
