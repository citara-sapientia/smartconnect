#!/usr/bin/python
#
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Funções para gerenciamento da câmera

from time import sleep
from datetime import datetime
from os import mkdir, path, system
from logger import log_info, panic, log_error, log_ok
import constants

try:
    from picamera import PiCamera
except ImportError:
    panic(f"error on {__file__} failed to import picamera module: {str(err)}")

class ImageFormats:
    """Contém os id's dos formatos de imagem
    """
    # Tipos de imagem
    JPEG = 0
    PNG = 1
    TIFF = 2
    GIF = 3
    BITMAP = 4
    SVG = 5


class Camera:
    """Operações com a câmera do raspberry
    """

    # Nome do arquivo padrão
    filename = "default.jpg"
    
    # Handler da câmera
    camera = None

    preview_started = False

    # Resolução padrão da imagem
    resolution = (300, 300)

    # Intervalo de captura
    delay = 5

    def __init__(self):
        """Inicializa a classe Camera, configura a resolução padrão e prepara o local de armazenamento das imagens
        """

        try:
            self.camera = PiCamera()
            self.camera.start_preview()
            self.preview_started = True
        except Exception as err:
            self.camera.close()
            panic("Erro ao abrir câmera: " + str(err))

        self.camera.resolution = self.resolution

        # Cria a pasta aonde os arquivos de imagem serão salvos
        if not path.exists(constants.images_folder):
            mkdir(constants.images_folder)

    # Configura a resolução da câmera    
    def configure_resulution(self, width=640, height=480):
        """Configura a resolução da imagem capturada pela câmera

        Keyword Arguments:
            width {int} -- [Largura] (default: {640})
            height {int} -- [Altura] (default: {480})
        """
        self.resolution = (width, height)

    def set_delay(self, delay_s=1):
        """Configura o tempo entre as capturas de imagem
        
        Keyword Arguments:
            delay_s {int} -- [Tempo de espera (segundos)]] (default: {1})
        """
        self.delay = delay_s

    # Captura uma imagem da câmera e salva num arquivo
    def capture_image(self):
        """Captura uma foto utilizando a câmera do raspberry

        Returns:
            [str] -- [nome do arquivo de imagem]
        """

        # A câmera precisa estar aberta
        if not self.preview_started:
            return ""

        # Gera o nome do caminho completo
        image_filename = self.generate_image_filename("jpg")
        full_path = constants.images_folder + "/" + image_filename

        #log_info("Salvando imagem em %s" % full_path)
    
        # Captura a imagem da câmera e salva num arquivo
        try:
            
            self.camera.capture(full_path)
        except Exception as err:
            log_error(f"Erro: {str(err)}")
            return ""

        return image_filename

    # Gera um nome de arquivo utilizando a data/hora atual
    def generate_image_filename(self, image_type="jpg"):
        """Gera um nome de arquivo de imagem utilizando
        a data/hora atual

        Keyword Arguments:
            image_type {str} -- [Tipo de imagem: "jpg", "png", "bmp", "tiff", "svg" ou "gif"] (default: {"jpg"})

        Returns:
            [str] -- [O nome do arquivo gerado]
        """
        # Obtém o instante atual
        n = datetime.now()

        # Gera o nome do arquivo
        filename = "picamera_%02d%02d%02d%02d%02d%02d.%s" % (n.day, n.month, n.year, n.hour, n.minute, n.second, image_type)

        return filename

    @staticmethod
    def get_image_id_from_filename(filename=""):
        """Obtém o ID da imagem especificada, de acordo com o seu tipo

        Keyword Arguments:
            filename {str} -- [nome do arquivo a ser analisado] (default: {""})

        Returns:
            [int] -- [ID da imagem]
        """
        image_id = ImageFormats.JPEG    # Formato padrão

        # Encontra a posição do "."
        index = filename.find(".")

        # Obtém a extensão
        file_ext = filename[index + 1:]

        if file_ext == "jpg":
            image_id = ImageFormats.JPEG
        elif file_ext == "gif":
            image_id = ImageFormats.GIF
        elif file_ext == "bmp":
            image_id = ImageFormats.BITMAP
        elif file_ext == "png":
            image_id = ImageFormats.PNG
        elif file_ext == "tiff":
            image_id = ImageFormats.TIFF
        elif file_ext == "svg":
            image_id = ImageFormats.SVG

        return image_id

    @staticmethod 
    def enable_camera(b_enable=True):

        flag = 0

        if b_enable is True:
            flag = 1

        data = '''# $1 is 0 to disable camera, 1 to enable it
        set_camera() {
        # Stop if /boot is not a mountpoint
        if ! mountpoint -q /boot; then
            return 1
        fi

        [ -e /boot/config.txt ] || touch /boot/config.txt

        if [ "$1" -eq 0 ]; then # disable camera
            set_config_var start_x 0 /boot/config.txt
            sed /boot/config.txt -i -e "s/^startx/#startx/"
            sed /boot/config.txt -i -e "s/^start_file/#start_file/"
            sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"
        else # enable camera
            set_config_var start_x 1 /boot/config.txt
            CUR_GPU_MEM=$(get_config_var gpu_mem /boot/config.txt)
            if [ -z "$CUR_GPU_MEM" ] || [ "$CUR_GPU_MEM" -lt 128 ]; then
            set_config_var gpu_mem 128 /boot/config.txt
            fi
            sed /boot/config.txt -i -e "s/^startx/#startx/"
            sed /boot/config.txt -i -e "s/^fixup_file/#fixup_file/"
        fi
        }

        set_camera %d
        ''' % (flag)

        script_name = "enable.sh"

        # Escreve o script no disco
        with open(script_name, 'w') as script:
            script.write(data)

        # Constrói o comando a ser executado
        command = "./" + script_name

        # executa o script usando o shell do sistema
        system(command)
