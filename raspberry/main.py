#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Código principal da unidade de controle


from button import Button
from logger import log_info, log_error, panic
from camera import Camera
from m_http import MiddlewayHttpClient
from m_tcp import MiddlewayTcpClient
from service import configure_service
from version import get_version
from threading import Thread
from sensors import Sensors
from time import sleep
from gpio import Gpio
import asyncio
import signal
import constants
import os

try:
    import RPi.GPIO as gpio
except ImportError:
    panic(f"error on {__file__} failed to import gpio module: {str(err)}")

# Verifica se o sistema atual é suportado
def check_os():
    if os.name != "posix":
        quit()

# Prepara o ambiente de execução
def prepare_workspace():
    if not os.path.exists(constants.sm_root_folder):
        os.mkdir(constants.sm_root_folder)

    if not os.path.exists(constants.images_folder):
        os.mkdir(constants.images_folder)

def main():
    not_stop = True

    # Id (cenário de mídia)
    media_scenario_id = "fa7c1fbb-8c02-4822-99dc-97357aac91b0"

    # Id (cenário de energia)
    energy_scenario_id = "9efe1e40-c4d8-4a4f-9114-3b3cc44d8dc8"

    #check_os
    log_info("Versão do software: " + get_version())

    # Prepare workspace
    prepare_workspace()

    log_info("Configurando serviços ...")

    # Se registra como um serviço do sistema
    try:
        configure_service()
    except Exception as err:
        print("Falha ao configurar serviço: " + str(err))
        quit()

    # Configuração global dos pinos de GPIO
    Gpio.configureMode()
    Gpio.configurePins()

    # Configura o botão de pânico
    panic_btn = Button()

    # Configura o handler do botão de pânico
    signal.signal(signal.SIGALRM, panic_btn.handle_panic_button)

    try:
        while not_stop is True:
            
            constants.cleanup_flag = False

            # Configura thread para leitura do botão
            panic_button_thread = Thread(target=panic_btn.handle_panic_button)
            panic_button_thread.start()

            # Configura o módulo de integração com os sensores
            sensors_thread = Thread(target=Sensors.handle_external_sensors)
            sensors_thread.start()

            log_info("Carregando módulo de atuação")
            device_state_changer_thread = Thread(target=MiddlewayTcpClient.change_device_state, args=(energy_scenario_id, ""))
            device_state_changer_thread.start()

            # Procurar por atualizações do software da unidade de controle
            log_info("Iniciando o módulo de captura de imagens ...")

            # Cria uma thread para o envio de imagens
            image_sender_thread = Thread(target=MiddlewayHttpClient.http_send_image_task, args=(media_scenario_id, 25))
            image_sender_thread.start()
            image_sender_thread.join()

            log_error("Reiniciando ...")
            constants.cleanup_flag = True

            sleep(5)
    except Exception as err:
        log_error(f"Erro fatal: {str(err)}")


# Execute program
while True:
    main()
