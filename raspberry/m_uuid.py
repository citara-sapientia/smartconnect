#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Autor: Lucas Vieira de Jesus <lucas.vieira@citara.io>
#
# Operações com UUID's (salvar, validar, etc)
#

from logger import log_info, log_error


class uuid:
    """Operações com UUID
    """

    # Tamanho mínimo do short id
    SHORT_ID_SIZE = 5

    # Tamanho máximo do id
    FULL_ID_SIZE = 36

    _uuid = ""

    def __init__(self, id=""):
        """Inicializa a classe uuid
        
        Keyword Arguments:
            id {str} -- [ID a ser usado] (default: {""})
        """

        self._uuid = str(id)

    def parse(self):
        """Valida o ID passado na função construtora
        
        Returns:
            [bool] -- [False se o ID estiver errado e True se estiver correto]
        """

        id_size = len(self._uuid)

        # Verifica o size
        if id_size != self.SHORT_ID_SIZE and id_size != self.FULL_ID_SIZE:
            return False

        # Analisa o UUID fornecido
        for c in self._uuid:
            try:
                if c == '-':
                    continue

                int(c, 16)
            except ValueError as e:
                log_error("uuid.parse(): " + str(e))
                return False

        return True
   