# -*- coding: utf-8 -*-
#
# Funções pare gerenciar o banco de dados local
#

import sqlite3
import os
from logger import log_error, log_ok
import m_uuid
from constants import sm_root_folder

class Database:

    # Nome do arquivo em disco
    database_filename = f"{sm_root_folder}/smconnect.db"

    # Flag
    db_initialized = False

    def __init__(self):
        # Setup database if necessary
        self.initialize_db()

    def initialize_db(self):

        query = "CREATE TABLE \"origin_by_scenario\" (\"scenario_id\" TEXT, \"origin_id\" TEXT, PRIMARY KEY(\"scenario_id\"));"

        # Verifica se o banco já existe
        if os.path.exists(self.database_filename):
            return True

        # Inicializa o banco
        db = self.connect_to_db()
        
        try:
            db.execute(query)
            db.commit()
        except Exception as error:
            log_error(f"Failed to create database: {str(error)}")

        log_ok("Database created successfully")

        # Fecha o banco de dados
        db.close()

        return db

    def connect_to_db(self):
        return sqlite3.connect(self.database_filename)

    def get_origin_id(self, scenario_id=""):

        # Id consistency        
        if m_uuid.uuid(scenario_id).parse() is False:
            return ""

        query = f"SELECT origin_id FROM origin_by_scenario WHERE scenario_id='{scenario_id}';"
        
        origin_id = ""

        # Open connection with database
        db = self.connect_to_db()

        # Extrai apenas o origin_id
        for row in db.execute(query):
            data = list(row)
            origin_id = str(data[0])

        return origin_id

    def save_origin_id(self, scenario_id="", origin_id=""):
        
        # Check UUID consistency
        scenario_id_validator = m_uuid.uuid(scenario_id)
        origin_id_validator = m_uuid.uuid(origin_id)

        if scenario_id_validator.parse() is False or origin_id_validator.parse() is False:
            return ""
        
        query = f"INSERT INTO origin_by_scenario VALUES ('{scenario_id}','{origin_id}');"
        
        # Open connection with database
        try:
            db = self.connect_to_db()
        except:
            return False

        # Execute query
        try:
            db.execute(query)
        except sqlite3.IntegrityError:
            return True

        # Save changes
        db.commit()

        # Close the connection
        db.close()

        return True
