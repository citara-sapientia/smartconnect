#!/usr/bin/python
#
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Gerencia os sensores externos (não ligados diretamente ao raspberry)

from serial_port import SerialPort, ModuleType
import constants
from m_tcp import MiddlewayTcpClient
from logger import log_error, log_info
from time import sleep


class Sensors:

    # Cliente TCP do middleway
    net_client = MiddlewayTcpClient()

    def __init__(self):
        pass
    
    def handle_dht11(self, dht_11_temperature="", dht_11_humidity=""):
        
        temperature = float.fromhex(dht_11_temperature) / 1000.0
        humidity = float.fromhex(dht_11_humidity) / 1000.0

        # TODO: # Se a temperatura for muito alta, a unidade de controle e gerenciamento deverá entrar em modo de espera
        if temperature > 80.0:
            log_error("very high temperature!")

        if humidity > 80.0:
            log_error("very high humidity")
        
        return 0

    def handle_hw484(self, hw484_data=""):
        
        # ID de destino dos dados do HW484
        scenario_id = "id"

        if len(hw484_data) != 24:
            log_error("Invalid audio data (length)")
            return 1

        # O dado vem sempre na sequência: atual, mínimo, máximo (cada um com 8 casas decimais)
        current = int(hw484_data[0:4])
        smaller = int(hw484_data[4:8])
        bigger = int(hw484_data[8:])

        # Constrói o payload
        payload = "%04x%04x%04x" % (smaller, bigger, current)
        
        # Converte o payload para um array de bytes
        packet = bytes(payload, 'utf-8')

        # Envia payload ao middleway
        self.net_client.send(scenario_id, packet)

        return 0

    @staticmethod
    def handle_external_sensors():
        
        sensors = Sensors()

        # Inicializa a comunicação serial (porta padrão)
        serial = SerialPort()

        # Abre a porta serial
        while serial.open_port() == False:
            if constants.cleanup_flag == True:
                break

            log_info("waiting for serial port ...")
            sleep(5)

        if constants.cleanup_flag is False:
            log_info("serial port is open")

        while constants.cleanup_flag is False:
            
            try:
                # Lê a temperatura
                temperature_data = serial.read_module_data(ModuleType.TEMPERATURE)

                # Lê a humidade
                humidity_data = serial.read_module_data(ModuleType.HUMIDITY)
                
                log_info(f"{temperature_data}{humidity_data}")

                # Lê o sensor de áudio
                audio_data = serial.read_module_data(ModuleType.MICROPHONE)
                
                # Trata o dado de cada sensor e realiza as ações necessárias
                if temperature_data != "" and humidity_data != "":
                    sensors.handle_dht11(temperature_data, humidity_data)
                
                if audio_data != "":
                    sensors.handle_hw484(audio_data)
            except IOError as IOErr:
                log_error(f"failed to read sensor: {str(IOErr)}")

        log_info("sensors handler: cleaning up ...")

        # Fecha a porta serial
        serial.close_port()
