#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Código para gerenciamento do botão de pânico

from gpio import Gpio
import signal
from time import sleep
from m_http import MiddlewayHttpClient
from logger import log_info, panic
import constants
import pins
from os import system

try:
    import RPi.GPIO as gpio
except ImportError:
    panic(f"error on {__file__} failed to import gpio module: {str(err)}")

class Button:

    @staticmethod
    def buttonPressed():
        """Verifica se o botão foi pressionado

        Retorno:
            [bool] -- [True se o botão foi pressionado, False no caso contrário]
        """

        # Lê o estado da chave
        return Gpio.digitalRead(pins.PANIC_BUTTON_PIN) == constants.HIGH

    @staticmethod
    def handle_panic_button():
        """Lê o botão de pânico constantemente
        """

        log_info("Lendo botão de pânico ...")

        # Lê o botão duas vezes por segundo
        while constants.cleanup_flag is False:
            
            # Dispara um alarme no processo de chamada
            pressed = Button.buttonPressed()
            
            if pressed:
                log_info("Playing sound ...")
                system(f"omxplayer {constants.MUSIC_FILE_PATH}")

            sleep(0.125)
        
        log_info("Panic button handler: exiting ...")
