#
# Script para instalar a unidade de controle e gerenciamento
#

install_folder="/bin/sc-control-unit"

function prepare_workspace()
{
    # Cria a pasta de instalação do programa, caso não exista
    if [ ! -d "$install_folder" ]; then
        echo "Criando pasta de instalação ..."
        mkdir $install_folder
    fi

    if [ "$?" != "0" ]; then
        echo "Não foi possível criar a pasta: '$install_folder'"
        exit 1
    fi

    if [ ! -e "main.py" ]; then
        # Vai para a pasta dos scripts
        cd ..
        if [ ! -e "main.py" ]; then
            echo "Os scripts não puderam ser localizados"
        fi
    fi

    # Copia todos os arquivos para a pasta de instalação
    cp *.py "$install_folder/" -vv
}

function install()
{
    if [ "$UID" != "0" ]; then
        echo "Execute este script como root"
    fi

    prepare_workspace
}

# Instala o software
install