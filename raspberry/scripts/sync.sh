#!/bin/bash

# Script para sincronizar o raspberry com o notebook

host=$1

if [ -z "$host" ]; then
        echo "Host not specified. Using default ..."
        host="10.101.35.104"
fi

echo "Host: $host"
echo "Updating files ..."
sshpass -v -p 'citara' scp root@$host:/home/citara/Desktop/Projetos/smartconnect/raspberry/*.py $PWD/
sshpass -v -p 'citara' scp root@$host:/home/citara/Desktop/Projetos/smartconnect/raspberry/scripts/*.sh $PWD/
echo "Files updated"