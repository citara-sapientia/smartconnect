#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Funções para gerenciamento dos pinos de GPIO

import constants
from time import sleep
from logger import panic
import pins

try:
    import RPi.GPIO as gpio
except ImportError as err:
    panic(f"error on {__file__} failed to import gpio module: {str(err)}")

class Gpio:

    """Operações de entrada e saída nos pinos digitais
    """

    PULL_UP = gpio.PUD_DOWN
    PULL_DOWN = gpio.PUD_UP

    @staticmethod
    def configureMode():

        # Configura o modo dos pinos
        gpio.setmode(gpio.BOARD)
        gpio.setwarnings(False)

    @staticmethod 
    def configurePins():
        
        # Botão de pânico
        Gpio.pinMode(pins.PANIC_BUTTON_PIN, constants.INPUT, Gpio.PULL_UP)

        # Relé
        Gpio.pinMode(pins.RELAY_PIN, constants.OUTPUT)

    @staticmethod
    def cleanup():
        """Libera os recursos utilizados
        """
        gpio.cleanup()

    @staticmethod
    def pinMode(pin, mode, pull_up_or_down=gpio.PUD_DOWN):
        """Configura o pino de GPIO

        Arguments:
            pin {int} -- Número do pino a ser configurado
            mode {int} -- Modo do pino (INPUT ou OUTPUT)

        Keyword Arguments:
            pull_up_or_down {int} -- Pull up ou Pull down (PUD_UP, PUD_DOWN) (default: {gpio.PUD_DOWN})
        """

        try:
            if mode == constants.INPUT:
                gpio.setup(pin, gpio.IN, pull_up_or_down)
            elif mode == constants.OUTPUT:
                gpio.setup(pin, gpio.OUT)
        except RuntimeError:
            print("Configure o modo antes de chamar esta função")
            quit()

    @staticmethod
    def digitalWrite(pin, level):
        """Muda o estado do pino especificado

        Arguments:
            pin {int} -- Número do pino
            level {int} -- Nível alto ou nível baixo (constants.HIGH ou constants.LOW)
        """
        if level == constants.LOW:
            gpio.output(pin, False)
        elif level == constants.HIGH:
            gpio.output(pin, True)

    @staticmethod
    def digitalRead(pin):
        """Lê o estado do pino especificado

        Arguments:
            pin {int} -- Número do pino

        Returns:
            int -- HIGH ou LOW
        """

        state = gpio.input(pin)

        return state
