#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Número dos pinos no modo BCM

import RPi.GPIO as gpio

# Pinos
GPIO2 = 3
GPIO3 = 5
GPIO4 = 7
GPIO17 = 11
GPIO27 = 13
GPIO22 = 15
GPIO23 = 16
GPIO10 = 19
GPIO9 = 21
GPIO11 = 23
GPIO0 = 27
GPIO5 = 29
GPIO6 = 31
GPIO13 = 33
GPIO19 = 35
GPIO26 = 37
GPIO14 = 8
GPIO15 = 10
GPIO18 = 12
GPIO23 = 16
GPIO24 = 18
GPIO25 = 22
GPIO8 = 24
GPIO7 = 26
GPIO1 = 28
GPIO12 = 32
GPIO16 = 36
GPIO20 = 38
GPIO21 = 40

# Níveis lógicos
LOW = 0     # Nível baixo
HIGH = 1    # Nível alto

# Modo dos pinos
INPUT = 2   # Entrada
OUTPUT = 3  # Saída

# UUID
SHORT_ID_LEN = 6
ID_LEN = 36
ID_LEN_WITH_NULL = 37

# Nome das pastas
sm_root_folder = "/bin/sc-control-unit"
images_folder = f"{sm_root_folder}/eye"

# Flag para verificar se a thread deve sair do loop
cleanup_flag = False

# String de autenticação
auth_string = "/auth"

#String para envio de payload
send_string = "/send"

# String para envio de heartbeat
beat_string = "/beat"

# Comandos de comunicação serial
read_temperature_command = "AT+TEMP"
read_humidity_command = "AT+HUM"
read_microphone_command = "AT+MIC"
error_command = "AT+ERR"

# Caminho do arquivo de áudio
MUSIC_FILE_PATH = f"{sm_root_folder}/sounds/beep.mp3"
