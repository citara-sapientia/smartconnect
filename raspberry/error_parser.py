#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Analisador de erros recebidos do firmware


class error_parser:
    """Analisador de erros devolvidos na porta serial
    """

    err_cmd = "AT+ERR"

    def parse_error(self, error_string):
        """Analisa um código de erro

        Arguments:
            error_string {[type]} -- [description]

        Returns:
            [int] -- [-1 em caso de erro ou qualquer outro valor maior ou igual a 0]
        """
        slash_found = False
        error = str(error_string)

        if len(error) <= 0:
            return -1

        # Verifica se o comando é um comando de erro
        if not error.startswith(self.err_cmd):
            return -1

        for ch in error:
            if ch is '/':
                slash_found = True
                break

        if not slash_found:
            return -1

        # Separa o comando do código de erro
        error_code = error.split("/")[1]

        # Retorna o erro como um decimal
        return int(error_code)
