#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Pinos utilizados

import constants

# Botão de pânico
PANIC_BUTTON_PIN = constants.GPIO25

# Relé
RELAY_PIN = constants.GPIO5
