# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Utilidades em geral
#

from logger import log_error
from base64 import b64encode
import py_compile


def b64_encode_file(filename="", binary_mode=True):
    """Codifica um arquivo para base64 e retorna um buffer

    Argumentos:
        filename {str} -- [Nome do arquivo] (default: {""})
        binary_mode {bool} -- [Modo binário] (default: {True})

    Returns:
        [str] -- [String com os dados do arquivo codificados para base64]
    """

    mode = "rb"

    if not binary_mode:
        mode = "r"

    # Abre o arquivo
    with open(filename, mode) as file:
        # Lê o arquivo todo
        data = file.read()

    # Certifica-se de que o arquivo será fechado
    if not file.closed:
        file.close()

    # Codifica para base64
    encoded = b64encode(data)

    return encoded.decode("utf-8")
