# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Funções para imprimir mensagens organizadas na tela
#

import os
import datetime


def time_str():

    # Obtém a hora atual
    _now = datetime.datetime.now()

    y = _now.year
    mt = _now.month
    d = _now.day
    h = _now.hour
    mi = _now.minute
    s = _now.second

    return "%02d/%02d/%04d %02d:%02d:%02d" % (d,mt,y,h,mi,s)


def print_red(text):
    """Escreve um texto na tela, em vermelho

    Argumentos:
        text {str} -- [Texto a ser exibio]
    """
    if os.name != 'nt':
        fmt = '\x1b[1;31m%s\x1b[0m' % text
        print(fmt)
    else:
        print(text)


def print_green(text):
    """Escreve um texto na tela, em verde

    Argumentos:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = '\x1b[1;32m%s\x1b[0m' % text
        print(fmt)
    else:
        print(text)


def print_yellow(text):
    """Escreve um texto na tela, em amarelo

    Argumentos:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = '\x1b[1;33m%s\x1b[0m' % text
        print(fmt)
    else:
        print(text)


def print_blue(text):
    """Escreve um texto na tela, em azul

    Argumentos:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = '\x1b[1;34m%s\x1b[0m' % text
        print(fmt)
    else:
        print(text)


def log_error(text):
    """Exibe uma mensagem de erro na tela

    Argumentos:
        text {str} -- [Texto a ser exibido]
    """

    if os.name != 'nt':
        fmt = f"\x1b[1;31m[{time_str()}]\x1b[0m "
        print(fmt + text)
    else:
        print(text)


def log_ok(text):
    """Exibe uma mensagem de sucesso
 
    Argumentos:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = f"\x1b[1;32m[{time_str()}]\x1b[0m "
        print(fmt + text)
    else:
        print(text)


def log_alert(text):
    """Exibe uma mensagem de alerta

    Argumentos:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = f"\x1b[1;33m[{time_str()}]\x1b[0m "
        print(fmt + text)
    else:
        print(text)


def log_info(text):
    """Exibe uma mensagem informativa
    
    Arguments:
        text {str} -- [Texto a ser exibido]
    """
    if os.name != 'nt':
        fmt = f"\x1b[1;34m[{time_str()}]\x1b[0m "
        print(fmt + text)
    else:
        print(text)


def panic(text):
    log_error(text)
    quit()


# Este script é um módulo e não deve ser executado diretamente
if __name__ == "__main__":
    quit()
