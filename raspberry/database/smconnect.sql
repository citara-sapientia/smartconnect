-- Query para criar a tabela
CREATE TABLE origin_by_scenario (
	"scenario_id"	TEXT,
	"origin_id"	TEXT,
	PRIMARY KEY("scenario_id")
);

-- Query para inserir o cenário no banco
INSERT INTO origin_by_scenario VALUES ('scenario_id','origin_id');

-- Query para obter um cenário já existente
SELECT origin_id FROM origin_by_scenario WHERE scenario_id='scenario_id';
