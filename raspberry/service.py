# -*- coding: utf-8 -*-
#
# Citara Labs Tecnologia
#
# Gerenciamento do serviço

import os
from logger import log_error

# Path dos arquivos de serviço
system_service_dir = "/etc/systemd/system"

# Nome do serviço
system_service_name = "sc-control-unit"

# Nome do arquivo de serviço da unidade de controle e gerenciamento
system_service_filename = system_service_name + ".service"

# Pasta de instalação do serviço
install_folder = "sc-control-unit"

# Apelido para o serviço
alias = "smartconnect"


# Checa o nível de privilégio no sistema
def we_have_root():
    """Verifica temos privilégios administrativos

    Returns:
        bool - True se for root, False caso contrário
    """
    return os.getuid() == 0


# Cria um serviço do sistema para a unidade de controle e gerenciamento
def create_system_service():
    """Registra um serviço do systemd
    """

    if not we_have_root():
        raise Exception("Precisamos de privilégios administrativos")

    # Caminho completo do arquivo de serviços
    service_path = system_service_dir + "/" + system_service_filename

    # Dados do arquivo de serviiço
    data = """[Unit]
Description=SmartConnect control and management unit (Core)

[Service]
Type=simple
ExecStart=/usr/bin/python3 /bin/%s/main.py
RestartSec=1
Restart=always
SuccessExitStatus=0

[Install]
WantedBy=multi_user.target
""" % (install_folder)

    try:

        # Verifica se o serviço já existe
        if os.path.exists(service_path):
            raise Exception("O serviço já existe")

        # Grava os dados no arquivo de serviço
        with open(service_path, "w") as service:
            service.write(data)

    except Exception as err:
        log_error("Erro ao criar serviço do sistema: " + str(err))
        return False

    return True


def update_system_service_config():
    """Recarrega o daemon do systemd
    """
    if not we_have_root():
        raise Exception("Precisamos de privilégios administrativos")

    os.system("systemctl daemon-reload")


def enable_service(service_name=system_service_name):
    """Habilita um serviço

    Keyword Arguments:
        service_name {str} --  Nome do serviço (default: {system_service_name})
    """
    if not we_have_root():
        raise Exception("Precisamos de privilégios administrativos")

    os.system("systemctl enable " + service_name)


def start_service(service_name=system_service_name):
    """Inicializa o serviço
    """
    if not we_have_root():
        raise Exception("Precisamos de privilégios administrativos")

    os.system("systemctl start " + system_service_name)


# Configura o serviço do sistema
def configure_service():
    """Configura o serviço da unidade de controle e gerenciamento
    
    Returns:
        [bool] -- [True: ok, False: erro]
    """

    if create_system_service() is True:

        # Habilita o serviço
        enable_service()

        # Atualiza o daemon
        update_system_service_config()

        # Inicia o serviço
        start_service()
    else:
        return False
