# Comunicação serial entre raspberry e arduino

- Fluxo de comunicação:
    - Raspberry envia request
    - Arduino lê e interpreta a request
    - Envia o resultado na porta serial

- A comunicação é bi-direcional (Arduino pode ler dados do Raspberry e vice-versa)

# Tabela de comandos

## Formato padrão dos comandos:
    comando/parâmetro

## Legenda:
    %04x - valor em hexadecimal (4 dígitos)
    %08x - valor em hexadecimal (8 dígitos)

| Dado a ser lido | Comando | Parâmetro     |
|-----------------|---------|---------------|
| Temperatura     | AT+TEMP | %04x          |
| Umidade         | AT+HUM  | %04x          |
| Microfone       | AT+MIC  | <%08x%08x%08x |

    
### Respostas
    - Em caso de erro, o seguinte dado é devolvido:
        AT+ERR/%04x
    - Se o código for 0, então a operação foi realizada com sucesso